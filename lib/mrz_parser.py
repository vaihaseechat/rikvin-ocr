'''
Created on 22-May-2018

@author: anirudh
'''
import json
import re
from datetime import datetime

countries = {}
countries["AFG"] = "Afghanistan";
countries["ALA"] = "Aland Islands";
countries["ALB"] = "Albania";
countries["DZA"] = "Algeria";
countries["ASM"] = "American Samoa";
countries["AND"] = "Andorra";
countries["AGO"] = "Angola";
countries["AIA"] = "Anguilla";
countries["ATA"] = "Antarctica";
countries["ATG"] = "Antigua and Barbuda";
countries["ARG"] = "Argentina";
countries["ARM"] = "Armenia";
countries["ABW"] = "Aruba";
countries["AUS"] = "Australia";
countries["AUT"] = "Austria";
countries["AZE"] = "Azerbaijan";
countries["BHS"] = "Bahamas";
countries["BHR"] = "Bahrain";
countries["BGD"] = "Bangladesh";
countries["BRB"] = "Barbados";
countries["BLR"] = "Belarus";
countries["BEL"] = "Belgium";
countries["BLZ"] = "Belize";
countries["BEN"] = "Benin";
countries["BMU"] = "Bermuda";
countries["BTN"] = "Bhutan";
countries["BOL"] = "Bolivia";
countries["BIH"] = "Bosnia and Herzegovina";
countries["BWA"] = "Botswana";
countries["BVT"] = "Bouvet Island";
countries["BRA"] = "Brazil";
countries["IOT"] = "British Indian Ocean Territory";
countries["BRN"] = "Brunei Darussalam";
countries["BGR"] = "Bulgaria";
countries["BFA"] = "Burkina Faso";
countries["BDI"] = "Burundi";
countries["KHM"] = "Cambodia";
countries["CMR"] = "Cameroon";
countries["CAN"] = "Canada";
countries["CPV"] = "Cape Verde";
countries["CYM"] = "Cayman Islands";
countries["CAF"] = "Central African Republic";
countries["TCD"] = "Chad";
countries["CHL"] = "Chile";
countries["CHN"] = "China";
countries["CXR"] = "Christmas Island";
countries["CCK"] = "Cocos (Keeling) Islands";
countries["COL"] = "Colombia";
countries["COM"] = "Comoros";
countries["COG"] = "Congo";
countries["COK"] = "Cook Islands";
countries["CRI"] = "Costa Rica";
countries["HRV"] = "Croatia";
countries["CUB"] = "Cuba";
countries["CYP"] = "Cyprus";
countries["CZE"] = "Czech Republic";
countries["PRK"] = "Democratic People's Republic of Korea";
countries["COD"] = "Democratic Republic of the Congo";
countries["DNK"] = "Denmark";
countries["DJI"] = "Djibouti";
countries["DMA"] = "Dominica";
countries["DOM"] = "Dominican Republic";
countries["TMP"] = "East Timor";
countries["ECU"] = "Ecuador";
countries["EGY"] = "Egypt";
countries["SLV"] = "El Salvador";
countries["GNQ"] = "Equatorial Guinea";
countries["ERI"] = "Eritrea";
countries["EST"] = "Estonia";
countries["ETH"] = "Ethiopia";
countries["FLK"] = "Falkland Islands (Malvinas)";
countries["FRO"] = "Faeroe Islands";
countries["FJI"] = "Fiji";
countries["FIN"] = "Finland";
countries["FRA"] = "France";
countries["FXX"] = "France, Metropolitan";
countries["GUF"] = "French Guiana";
countries["PYF"] = "French Polynesia";
countries["GAB"] = "Gabon";
countries["GMB"] = "Gambia";
countries["GEO"] = "Georgia";
countries["D"] = "Germany";
countries["GHA"] = "Ghana";
countries["GIB"] = "Gibraltar";
countries["GRC"] = "Greece";
countries["GRL"] = "Greenland";
countries["GRD"] = "Grenada";
countries["GLP"] = "Guadeloupe";
countries["GUM"] = "Guam";
countries["GTM"] = "Guatemala";
countries["GIN"] = "Guinea";
countries["GNB"] = "Guinea-Bissau";
countries["GUY"] = "Guyana";
countries["HTI"] = "Haiti";
countries["HMD"] = "Heard and McDonald Islands";
countries["VAT"] = "Holy See (Vatican City State)";
countries["HND"] = "Honduras";
countries["HKG"] = "Hong Kong";
countries["HUN"] = "Hungary";
countries["ISL"] = "Iceland";
countries["IND"] = "India";
countries["IDN"] = "Indonesia";
countries["IRN"] = "Iran, Islamic Republic of";
countries["IRQ"] = "Iraq";
countries["IRL"] = "Ireland";
countries["ISR"] = "Israel";
countries["ITA"] = "Italy";
countries["JAM"] = "Jamaica";
countries["JPN"] = "Japan";
countries["JOR"] = "Jordan";
countries["KAZ"] = "Kazakhstan";
countries["KEN"] = "Kenya";
countries["KIR"] = "Kiribati";
countries["KWT"] = "Kuwait";
countries["KGZ"] = "Kyrgyzstan";
countries["LAO"] = "Lao People's Democratic Republic";
countries["LVA"] = "Latvia";
countries["LBN"] = "Lebanon";
countries["LSO"] = "Lesotho";
countries["LBR"] = "Liberia";
countries["LBY"] = "Libyan Arab Jamahiriya";
countries["LIE"] = "Liechtenstein";
countries["LTU"] = "Lithuania";
countries["LUX"] = "Luxembourg";
countries["MDG"] = "Madagascar";
countries["MWI"] = "Malawi";
countries["MYS"] = "Malaysia";
countries["MDV"] = "Maldives";
countries["MLI"] = "Mali";
countries["MLT"] = "Malta";
countries["MHL"] = "Marshall Islands";
countries["MTQ"] = "Martinique";
countries["MRT"] = "Mauritania";
countries["MUS"] = "Mauritius";
countries["MYT"] = "Mayotte";
countries["MEX"] = "Mexico";
countries["FSM"] = "Micronesia, Federated States of";
countries["MCO"] = "Monaco";
countries["MNG"] = "Mongolia";
countries["MNE"] = "Montenegro";
countries["MSR"] = "Montserrat";
countries["MAR"] = "Morocco";
countries["MOZ"] = "Mozambique";
countries["MMR"] = "Myanmar";
countries["NAM"] = "Namibia";
countries["NRU"] = "Nauru";
countries["NPL"] = "Nepal";
countries["NLD"] = "Netherlands, Kingdom of the";
countries["ANT"] = "Netherlands Antilles";
countries["NTZ"] = "Neutral Zone";
countries["NCL"] = "New Caledonia";
countries["NZL"] = "New Zealand";
countries["NIC"] = "Nicaragua";
countries["NER"] = "Niger";
countries["NGA"] = "Nigeria";
countries["NIU"] = "Niue";
countries["NFK"] = "Norfolk Island";
countries["MNP"] = "Northern Mariana Islands";
countries["NOR"] = "Norway";
countries["OMN"] = "Oman";
countries["PAK"] = "Pakistan";
countries["PLW"] = "Palau";
countries["PSE"] = "Palestine";
countries["PAN"] = "Panama";
countries["PNG"] = "Papua New Guinea";
countries["PRY"] = "Paraguay";
countries["PER"] = "Peru";
countries["PHL"] = "Philippines";
countries["PCN"] = "Pitcairn";
countries["POL"] = "Poland";
countries["PRT"] = "Portugal";
countries["PRI"] = "Puerto Rico";
countries["QAT"] = "Qatar";
countries["KOR"] = "Republic of Korea";
countries["MDA"] = "Republic of Moldova";

countries["ROU"] = "Romania";
countries["RUS"] = "Russian Federation";
countries["RWA"] = "Rwanda";
countries["SHN"] = "Saint Helena";
countries["KNA"] = "Saint Kitts and Nevis";
countries["LCA"] = "Saint Lucia";
countries["SPM"] = "Saint Pierre and Miquelon";
countries["VCT"] = "Saint Vincent and the Grenadines";
countries["WSM"] = "Samoa";
countries["SMR"] = "San Marino";
countries["STP"] = "Sao Tome and Principe";
countries["SAU"] = "Saudi Arabia";
countries["SRB"] = "Serbia";
countries["SEN"] = "Senegal";
countries["SYC"] = "Seychelles";
countries["SLE"] = "Sierra Leone";
countries["SGP"] = "Singapore";
countries["SVK"] = "Slovakia";
countries["SVN"] = "Slovenia";
countries["SLB"] = "Solomon Islands";
countries["SOM"] = "Somalia";
countries["ZAF"] = "South Africa";
countries["SGS"] = "South Georgia and the South Sandwich Island";
countries["SSD"] = "South Sudan";
countries["ESP"] = "Spain";
countries["LKA"] = "Sri Lanka";
countries["SDN"] = "Sudan";
countries["SUR"] = "Suriname";
countries["SJM"] = "Svalbard and Jan Mayen Islands";
countries["SWZ"] = "Swaziland";
countries["SWE"] = "Sweden";
countries["CHE"] = "Switzerland";
countries["SYR"] = "Syrian Arab Republic";
countries["TWN"] = "Taiwan Province of China";
countries["TJK"] = "Tajikistan";
countries["TLS"] = "Timor Leste";
countries["THA"] = "Thailand";
countries["MKD"] = "The former Yugoslav Republic of Macedonia";
countries["TGO"] = "Togo";
countries["TKL"] = "Tokelau";
countries["TON"] = "Tonga";
countries["TTO"] = "Trinidad and Tobago";
countries["TUN"] = "Tunisia";
countries["TUR"] = "Turkey";
countries["TKM"] = "Turkmenistan";
countries["TCA"] = "Turks and Caicos Islands";
countries["TUV"] = "Tuvalu";
countries["UGA"] = "Uganda";
countries["UKR"] = "Ukraine";
countries["ARE"] = "United Arab Emirates";
countries["GBR"] = "United Kingdom of Great Britain and Northern Ireland Citizen";
countries["GBD"] = "United Kingdom of Great Britain and Northern Ireland Dependent Territories Citizen";
countries["GBN"] = "United Kingdom of Great Britain and Northern Ireland National (oversees)";
countries["GBO"] = "United Kingdom of Great Britain and Northern Ireland Oversees Citizen";
countries["GBP"] = "United Kingdom of Great Britain and Northern Ireland Protected Person";
countries["GBS"] = "United Kingdom of Great Britain and Northern Ireland Subject";
countries["TZA"] = "United Republic of Tanzania";
countries["USA"] = "United States of America";
countries["UMI"] = "United States of America Minor Outlying Islands";
countries["URY"] = "Uruguay";
countries["UZB"] = "Uzbekistan";
countries["VUT"] = "Vanuatu";
countries["VEN"] = "Venezuela";
countries["VNM"] = "Viet Nam";
countries["VGB"] = "Virgin Islands (Great Britian)";
countries["VIR"] = "Virgin Islands (United States)";
countries["WLF"] = "Wallis and Futuna Islands";
countries["ESH"] = "Western Sahara";
countries["YEM"] = "Yemen";
countries["ZAR"] = "Zaire";
countries["ZMB"] = "Zambia";
countries["ZWE"] = "Zimbabwe";
countries["UNO"] = "United Nations Organization Official";
countries["UNA"] = "United Nations Organization Specialized Agency Official";
countries["XAA"] = "Stateless (per Article 1 of 1954 convention)";
countries["XXB"] = "Refugee (per Article 1 of 1951 convention, amended by 1967 protocol)";
countries["XXC"] = "Refugee (non-convention)";
countries["XXX"] = "Unspecified / Unknown";





# VALUES FOR EACH varTER, USED TO PERFORM THE CHECK DIGITS VERIFICATION */
checkDigitValues = {};
checkDigitValues["<"] = 0;
checkDigitValues["A"] = 10;
checkDigitValues["B"] = 11;
checkDigitValues["C"] = 12;
checkDigitValues["D"] = 13;
checkDigitValues["E"] = 14;
checkDigitValues["F"] = 15;
checkDigitValues["G"] = 16;
checkDigitValues["H"] = 17;
checkDigitValues["I"] = 18;
checkDigitValues["J"] = 19;
checkDigitValues["K"] = 20;
checkDigitValues["L"] = 21;
checkDigitValues["M"] = 22;
checkDigitValues["N"] = 23;
checkDigitValues["O"] = 24;
checkDigitValues["P"] = 25;
checkDigitValues["Q"] = 26;
checkDigitValues["R"] = 27;
checkDigitValues["S"] = 28;
checkDigitValues["T"] = 29;
checkDigitValues["U"] = 30;
checkDigitValues["V"] = 31;
checkDigitValues["W"] = 32;
checkDigitValues["X"] = 33;
checkDigitValues["Y"] = 34;
checkDigitValues["Z"] = 35;


#print(str(len("PUSAKIM<<CHRISTINE<M<<<<<<<<<<<<<<<<<<<<<<<")))

def normalize_data_td3(full_mrz_lines):
    
    line=full_mrz_lines[0]
    line=line.replace(" ","")
    if(len(line)<44):
        
        if (line[1]!="<"  and (line[2:5] not in countries.keys()) ):
            line=line[0]+"<"+line[1:]  
        
        if (line[2:5] not in countries.keys()):
            print(line)
            print(line[2:5])
            print(" Country not detected")
            return None
        while(len(line)<44):
            line=line+"<"
    full_mrz_lines[0]=line
    line1=full_mrz_lines[1]
    line1=line1.replace(" ","")
    if (len(line1)!=44):
        
        if (line1[10:13] not in countries.keys()):
            
            print(line)
            print(line1)
            print(line1[10:13])
            print(" Country not detected")
            return None

        if line1[13:19].isnumeric() is False:
           
            print(line)
            print(line1)
            print(line1[13:19])
            print(" invalid dob")
            return None
        
        
        
    full_mrz_lines[1]=line1
        
    return full_mrz_lines

def detect_mrz_lines(full_text):

    full_mrz_lines=None

    lines=full_text.split("\n")
    if len(lines) ==0:
        

        return  full_mrz_lines

    for i in range (0,len(lines)-1):
       
        if ((len(lines[i].replace(" ","")) >40 or (len(lines[i].replace(" ","")) >20 and "<<" in lines[i].replace(" ",""))) and len(lines[i].replace(" ",""))<=48) is False:
            
            continue
        
        if (lines[i][0]=="P" and "<<" in lines[i]):
            full_mrz_lines=[]
            full_mrz_lines.append(lines[i])
            full_mrz_lines.append(lines[i+1])
            return normalize_data_td3(full_mrz_lines)
            
        
    return full_mrz_lines

def parse_mrz(full_text):

    output={}
   
    full_mrz_lines=detect_mrz_lines(full_text)
    if full_mrz_lines==None:
        return None
    
    if (full_mrz_lines[0][2:5] in countries): 
        output["CountryofIssue"]=countries[full_mrz_lines[0][2:5]].replace("<","") 
    
    if (full_mrz_lines[1][10:13] in countries):
        output["Nationality"]=countries[full_mrz_lines[1][10:13]].replace("<","")
        
    output["DateofBirth"]=full_mrz_lines[1][13:19].replace("<","")
    output["DateofBirth"]=convertDateToyyyy(validateDate(output["DateofBirth"][-2:]+"-"+output["DateofBirth"][-4:-2]+"-"+output["DateofBirth"][-6:-4]).replace("<","")) 
    
    
    
    output["PassportNumber"]=full_mrz_lines[1][0:9].replace("<","")
    
    output["DateofExpiry"]=full_mrz_lines[1][21:27].replace("<","")
    output["DateofExpiry"]=convertDateToyyyy(validateDate(output["DateofExpiry"][-2:]+"-"+output["DateofExpiry"][-4:-2]+"-"+output["DateofExpiry"][-6:-4]).replace("<","")) 
    
    names_raw=full_mrz_lines[0][5:44]
    upper_limit=43
    for i in range(len(names_raw)-1,0,-1):
       
        if(names_raw[i]!="<"):
            upper_limit=i
            break
            
        
    all_names=names_raw[0:(upper_limit+1)].split("<<")
    output["all_names"]=names_raw.replace("<"," ").strip()

   
    if(len(all_names)==2):
        output["Surname"]=all_names[0].replace("<","") 
        output["GivenName"]=all_names[1].replace("<"," ").strip()
    else:
        name_list=output["all_names"].split(" ")
        if len(name_list)>1:
            output["Surname"]=name_list[0].replace("<","") 
            output["GivenName"]=""
            for j in range (1,len(name_list)):
                output["GivenName"]=output["GivenName"]+" "+name_list[j]
            output["GivenName"]=output["GivenName"].strip().replace("<","")     
            
    output["Sex"]=""
    if(full_mrz_lines[1][20]=="M"):
        output["Sex"]="Male"
    elif full_mrz_lines[1][20]=="F":
        output["Sex"]="Female"
    
    output["PlaceofBirth"]=getPlaceOfBirth(full_text).replace("<","") 
    output["DateofIssue"]=getDateofIssue(full_text).replace("<","") 
    
    return output


def parse_passeye_mrz(pe_mrz):
    
    if(pe_mrz is None):
        return None
    output={} 
    output["Nationality"]=pe_mrz.nationality.replace("<","") 
    output["DateofBirth"]=pe_mrz.date_of_birth.replace("<","") 
    output["DateofBirth"]=convertDateToyyyy(validateDate(output["DateofBirth"][-2:]+"-"+output["DateofBirth"][-4:-2]+"-"+output["DateofBirth"][-6:-4]).replace("<","")) 
    
    
    output["PassportNumber"]=pe_mrz.number.replace("<","") 
    output["DateofExpiry"]=pe_mrz.expiration_date.replace("<","") 
    output["DateofExpiry"]=convertDateToyyyy(validateDate(output["DateofExpiry"][-2:]+"-"+output["DateofExpiry"][-4:-2]+"-"+output["DateofExpiry"][-6:-4]).replace("<","")) 
    output["Sex"]=""
    if(pe_mrz.sex =="M"):
        output["Sex"]="Male"
    elif pe_mrz.sex =="F":
        output["Sex"]="Female"
    output["Surname"]=pe_mrz.surname.replace("<","")  
    output["GivenName"]=pe_mrz.names.replace("<","") 
    output["all_names"]=(pe_mrz.surname +" "+pe_mrz.names).replace("<","") 
    return output
 
def getPlaceOfBirth(full_text):    
    text_lines=full_text.split('\n')
    for i in range (0,len(text_lines)):
        text_line=text_lines[i]             
        if(('birth' in text_line.lower() or 'domicile' in text_line.lower() ) and i != len(text_lines)-1):
            if(text_lines[i+1].count(" ") <3 and hasNumbers(text_lines[i+1]) is False):
                return text_lines[i+1].strip()
    return ""


def getDateofIssue(full_text):  
    text_lines=full_text.split('\n')
    for i in range (0,len(text_lines)):
        text_line=text_lines[i]             
        if('birth' in text_line.lower() and i != len(text_lines)-1):
            if(hasNumbers(text_lines[i+1]) is True):
                
                date=None
                match = re.search(r'\d{2}/\d{2}/\d{4}', text_lines[i+1])
                if(match==None):
                    match = re.search(r'\d{2} [a-zA-Z]{3} \d{4}', text_lines[i+1])
                    if(match==None):
                        match1 = re.search(r'\d{2} [a-zA-Z]{3}', text_lines[i+1])
                        match2 = re.search(r'[a-zA-Z]{3} \d{2}', text_lines[i+1])
                        if(match1!=None and match2!=None):
                            full_date_matched= match1.group()+" "+match2.group().split(" ")[1]
                            date= datetime.strptime(full_date_matched, '%d %b %y').date()
                    else:
                        date= datetime.strptime(match.group(), '%d %b %Y').date()    
                else:
                    date = datetime.strptime(match.group(), '%d/%m/%Y').date()    
                if(date!=None):         
                    return str(date.strftime('%d-%m-%Y'))
    return ""
      

    

def validateDate(dateString):
    
    numbers = sum(c.isdigit() for c in dateString)
    if(numbers==6):
        return dateString
    
    return ""

def convertDateToyyyy(inputDateFull):
    
    if(inputDateFull==""):
        return ""
    
    lastTwoDigit=int(inputDateFull.split("-")[-1])
    if(lastTwoDigit>int(datetime.today().strftime('%y'))+15):
        
        return inputDateFull[0:inputDateFull.rfind('-')+1]+str(int(int(datetime.today().strftime('%Y'))/100)-1)+str(lastTwoDigit)
    
    
        
    return inputDateFull[0:inputDateFull.rfind('-')+1]+str(int(int(datetime.today().strftime('%Y'))/100))+str(lastTwoDigit)
    
    

def hasNumbers(inputString):
    return any(char.isdigit() for char in inputString)
