'''
Created on 21-May-2018

@author: anirudh
'''


import io
import os
import sys
from lib import parsepdfimage,docparser
from lib import mrz_parser
from os import listdir
from os.path import isfile, join
import pytesseract
from passporteye import read_mrz
from google.cloud import vision
from google.cloud.vision import types
from builtins import input
import json

try:
    import Image
except ImportError:
    from PIL import Image





myImage="tmp_ocr.png"
pytesseract.pytesseract.tesseract_cmd = '/usr/bin/tesseract'
supported_formats=["png","pdf","jpg","jpeg","bmp","tiff"]
# Instantiates a client
os.environ["GOOGLE_APPLICATION_CREDENTIALS"] ='/var/www/html/rikvinapi/Cloud Vision OCR-3544d9e1e57e.json'
def get_image_pdf(content_act_arr,file_name):

    # Loads the image into memory
    print("Checking for PDF File") 
    with io.open(file_name, 'rb') as image_file:
        content_act_arr.append( image_file.read())
    ext_val="."+file_name.split(".")[-1]
    if (file_name.split(".")[-1].lower()=="pdf"):
        print("Getting Image from PDF File")
        content_act_arr,parse_pdf=parsepdfimage.get_image_bytes_pd(content_act_arr[0])
        ext_val=".png"
    else:
        parse_pdf=True  
    return content_act_arr   
        

def parse_sing_id_front_main(file_name):
    client = vision.ImageAnnotatorClient()

    print(file_name)
    output_json=None
    content_act_arr=[]
    content_act_arr=get_image_pdf(content_act_arr,file_name)
    if(content_act_arr is not None and len(content_act_arr)!=0):
        print("\nlength of content array: "+str(len(content_act_arr)))
        for content_act in content_act_arr:
              
            if len(content_act)<10000000:
                
                
                image = types.Image(content=content_act)
                # Performs label detection on the image file
                response = client.document_text_detection(image=image)
                document = response.full_text_annotation
                print(document.text)
                output_json= docparser.parse_nric(''.join(i for i in document.text if ord(i)<128),output_json)
                
                if("DateofBirth" not in  output_json or "CountryofBirth" not in output_json or "Sex" not in output_json or "NRICnumber" not in output_json):
                    print(" Trying tesseract \n")
                    output_json= docparser.parse_nric(pytesseract.image_to_string(Image.open(file_name)),output_json)
                
            else:
                return ('{"Error":"File Size too large"}')
    if output_json is None or len(output_json)==0:        
        return '{"Error":"Poor Image Quality"}'      
    
    return json.dumps(output_json)


def parse_sing_id_back_main(file_name):
    client = vision.ImageAnnotatorClient()

    print(file_name)
    output_json=None
    content_act_arr=[]
    content_act_arr=get_image_pdf(content_act_arr,file_name)
    if(content_act_arr is not None and len(content_act_arr)!=0):
        print("\nlength of content array: "+str(len(content_act_arr)))
        for content_act in content_act_arr:
              
            if len(content_act)<10000000:
                
                
                image = types.Image(content=content_act)
                # Performs label detection on the image file
                response = client.document_text_detection(image=image)
                document = response.full_text_annotation
                print(document.text)
                output_json= docparser.parse_nric(''.join(i for i in document.text if ord(i)<128),output_json)
                
                if( "Address" not in output_json):
                    print(" Trying tesseract \n")
                    output_json= docparser.parse_nric(pytesseract.image_to_string(Image.open(file_name)),output_json)
                
            else:
                return ('{"Error":"File Size too large."}')
    if output_json is None or len(output_json)==0:        
        return '{"Error":"Poor Image Quality"}'      
    
    return json.dumps(output_json)


def parse_sing_ep_front_main(file_name):
    client = vision.ImageAnnotatorClient()

    print(file_name)
    output_json=None
    content_act_arr=[]
    content_act_arr=get_image_pdf(content_act_arr,file_name)
    if(content_act_arr is not None and len(content_act_arr)!=0):
        print("\nlength of content array: "+str(len(content_act_arr)))
        for content_act in content_act_arr:
              
            if len(content_act)<10000000:
                image = types.Image(content=content_act)
                # Performs label detection on the image file
                response = client.document_text_detection(image=image)
                document = response.full_text_annotation
                print(''.join(i for i in document.text if ord(i)<128))
                output_json= docparser.parse_ep_front(''.join(i for i in document.text if ord(i)<128),output_json)
                
                if("FIN" not in output_json or "Employer" not in output_json):
                    print(" Trying tesseract \n")
                    output_json= docparser.parse_nric(pytesseract.image_to_string(Image.open(file_name)),output_json)
                
                if(output_json is not None and len(output_json)!=0):
                    return json.dumps(output_json)
            else:
                return ('{"Error":"File Size too large."}')       
    return '{"Error":"Poor Image Quality"}' 

def parse_sing_ep_back_main(file_name):
    client = vision.ImageAnnotatorClient()

    print(file_name)
    output_json=None
    content_act_arr=[]
    content_act_arr=get_image_pdf(content_act_arr,file_name)
    if(content_act_arr is not None and len(content_act_arr)!=0):
        print("\nlength of content array: "+str(len(content_act_arr)))
        for content_act in content_act_arr:
              
            if len(content_act)<10000000:
                image = types.Image(content=content_act)
                # Performs label detection on the image file
                response = client.document_text_detection(image=image)
                document = response.full_text_annotation
                print(''.join(i for i in document.text if ord(i)<128))
                output_json= docparser.parse_ep_back(''.join(i for i in document.text if ord(i)<128),output_json)

                
                if("Sex" not in output_json or "Nationality" not in output_json or "Occupation" not in output_json or "DateofExpiry" not in output_json or "DateofIssue" not in output_json):
                    print(" Trying tesseract \n")
                    output_json= docparser.parse_nric(pytesseract.image_to_string(Image.open(file_name)),output_json)
                    
                if(output_json is not None and len(output_json)!=0):
                    return json.dumps(output_json)
            else:
                return ('{"Error":"File Size too large."}')       
    return '{"Error":"Poor Image Quality"}' 





def parse_mrz_main(file_name):
    client = vision.ImageAnnotatorClient() 
    # The name of the image file to annotate
    parse_pdf=False
    other_error="Invalid MRZ"
    output_json=None
    content_act_arr=[]
    content_act_arr=get_image_pdf(content_act_arr,file_name)
          
    if(content_act_arr is not None and len(content_act_arr)!=0):
        print("\nlength of content array: "+str(len(content_act_arr)))
        for content_act in content_act_arr:
              
            if len(content_act)<10000000:
                print("Content length " + str(len(content_act)))
                image = types.Image(content=content_act)
                # Performs label detection on the image file
                response = client.document_text_detection(image=image)
                document = response.full_text_annotation
                print(''.join(i for i in document.text if ord(i)<128))
                output_json= mrz_parser.parse_mrz(''.join(i for i in document.text if ord(i)<128))
                
                if(output_json is None):
                    t_image=os.path.splitext(file_name)[0]+myImage
                    with open(t_image,"wb") as fw:
                        fw.write(content_act)
                    try:    
                        mrz = read_mrz(t_image)
                        output_json= mrz_parser.parse_passeye_mrz(mrz)
                        os.remove(t_image)
                        if output_json is not None:
                            return json.dumps(output_json)
                    except Exception as e:
                        print(str(e))
                        
                else:
           
                    return json.dumps(output_json)
            else:
                return ('{"Error":"File Size too large."}')       
    return '{"Error":"Poor Image Quality"}'       
            

