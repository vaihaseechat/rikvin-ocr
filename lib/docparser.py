'''
Created on 11-Jun-2018

@author: anirudh
'''
import difflib
from datetime import datetime

def parse_nric(text,output_json):
   
    print(text)
    text_data=text.split("\n")
    key_nric=["race","date of birth","country of birth","nric no.","date of issue"]
    dataset={}
    if output_json is not None:
        dataset=output_json
    
    
    for i in range (0, len(text_data)):
        
        
        data=text_data[i].lower()
        word_list=data.strip().split(' ')
        
        if "identity card" in data or (len(data)>14 and difflib.SequenceMatcher(None,'identity card',data[0:13]).quick_ratio()>0.8) and "NRICnumber" not in dataset:
            if(text_data[i].split()[-1] not in key_nric):
                if(text_data[i].split()[-1][0].isdigit() is False):
                    dataset["NRICnumber"]= text_data[i].split()[-1].replace('$','S')
                
        elif ("name" == data or difflib.SequenceMatcher(None,'name',data).quick_ratio()>0.7) and i!=len(text_data)-1 and "Name" not in dataset:
            if(text_data[i+1] not in key_nric):
                dataset["Name"]= text_data[i+1]
        elif  ("race" == data or difflib.SequenceMatcher(None,'race',data).quick_ratio()>0.7) and i!=len(text_data)-1 and "Race" not in dataset:
            if(text_data[i+1] not in key_nric):
                dataset["Race"]=text_data[i+1]
        elif  ("date of birth" == data or difflib.SequenceMatcher(None,'date of birth',data).quick_ratio()>0.7) and i!=len(text_data)-1 and "DateofBirth" not in dataset:
            
            if(len(text_data[i+1].split())!=0):
                if(text_data[i+1].split()[0] not in key_nric):
                    dataset["DateofBirth"]=validateDate(text_data[i+1].split()[0])
        elif  ("sex" == data or difflib.SequenceMatcher(None,'sex',data).quick_ratio()>0.5) and i!=len(text_data)-1 and "Sex" not in dataset:
            if(len(text_data[i+1].strip())==1):
                dataset["Sex"]=text_data[i+1]    
        elif (len(data.strip())==1) and "Sex" not in dataset:
            if(data == "m"):
                dataset["Sex"]="Male"
            elif(data =='f'):
                dataset["Sex"]="Female"  
        elif (('m' in word_list)) and "Sex" not in dataset: 
            dataset["Sex"]="Male"
        elif (('f' in word_list)) and "Sex" not in dataset: 
            dataset["Sex"]="Female"                
        elif  ("country of birth" == data or difflib.SequenceMatcher(None,'country of birth',data).quick_ratio()>0.7) and i!=len(text_data)-1 and "CountryofBirth" not in dataset:
            if(text_data[i+1] not in key_nric):
                dataset["CountryofBirth"]=text_data[i+1]    
        elif  "nric no." in data or (len(data)>9 and difflib.SequenceMatcher(None,'nric no.',data[0:8]).quick_ratio()>0.8) and "NRICnumber" not in dataset:
            if(len(text_data[i].split())!=0):
                if(text_data[i].split()[-1]  not in key_nric):
                    dataset["NRICnumber"]=text_data[i].split()[-1].replace('$','S')  
        elif  ("date of issue" == data or difflib.SequenceMatcher(None,'date of issue',data).quick_ratio()>0.8) and i!=len(text_data)-1 and "DateofIssue" not in dataset:
            if(len(text_data[i+1].split())!=0):
                if(text_data[i+1].split()[-1] not in key_nric):
                    dataset["DateofIssue"]=validateDate(text_data[i+1].split()[-1])
        elif  ("date of issue" in data or difflib.SequenceMatcher(None,'blood group date of issue',data).quick_ratio()>0.8) and i!=len(text_data)-1 and ("DateofIssue" not in dataset):
            if(len(text_data[i+1].split())!=0):
                if(text_data[i+1].split()[-1] not in key_nric):
                    dataset["DateofIssue"]=validateDate(text_data[i+1].split()[-1])            
        elif  (("address" == data or difflib.SequenceMatcher(None,'address',data).quick_ratio()>0.7) and i!=len(text_data)-1 and "Address" not in dataset) or ( "DateofIssue" in dataset and dataset["DateofIssue"].lower() in  data and i!=len(text_data)-1 and "address" not in text_data[i+1].lower() and (len(text_data)-i) <8):
            add_full=""
            add_full_arr=[]
            for add in range (i+1, len(text_data)):
                if(text_data[add] in key_nric):
                    
                    break
                if('singapore' in text_data[add].lower()):
                    add_full+=text_data[add]
                    add_full_arr.append(text_data[add])
                    add_full+="\n"
                    break
                add_full+=text_data[add]
                add_full_arr.append(text_data[add])
                add_full+="\n"
            dataset["Address"]=add_full  
            if(add_full!=""):
                if(len(add_full_arr)==2):
                    address_data_line2=add_full_arr[1].split(" ")
                    if(len(address_data_line2)==2):
                        dataset["Postal"]=address_data_line2[len(address_data_line2)-1]
                    if(len(address_data_line2)>2 and '#' in address_data_line2[0] and '-' in address_data_line2[0]):
                        #dataset["FloorandUnit"]=address_data_line2[0]
                        dataset["Floor"]=address_data_line2[0].split("-")[0].replace("#","")
                        dataset["Unit"]=address_data_line2[0].split("-")[1]
                        dataset["Postal"]=address_data_line2[len(address_data_line2)-1]  
                    else:
                        dataset["Postal"]=address_data_line2[len(address_data_line2)-1]
                            
                    if('#' in add_full_arr[0] and add_full_arr[0][0] !="#" ):    
                        address_data_line1= add_full_arr[0].split("#")
                        dataset["Street"] =address_data_line1[0].strip() 
                        dataset["Unit"]="#"+address_data_line1[1] 
                        if('-' in address_data_line1[1]):
                            dataset["Floor"]=address_data_line1[1].split("-")[0].replace("#","")
                            dataset["Unit"]=address_data_line1[1].split("-")[1]
                    else:     
                        dataset["Street"]=add_full_arr[0].strip()
                        
                if(len(add_full_arr)==3):
                    address_data_line3=add_full_arr[2].split(" ")
                    if(len(address_data_line3)!=0):
                        dataset["Postal"]=address_data_line3[len(address_data_line3)-1]
                    if( add_full_arr[1][0] =="#" ):
                        if('-' in add_full_arr[1]):
                            dataset["Floor"]=add_full_arr[1].split("-")[0].replace("#","")
                            dataset["Unit"]=add_full_arr[1].split("-")[1]     
                        else:
                            dataset["Unit"]=add_full_arr[1].strip()
                    dataset["Street"]=add_full_arr[0].strip()            
            
    print(str(dataset))
    return dataset






def parse_ep_front(text,output_json):
   
    print(text)
    text_data=text.split("\n")
    #key_nric=["fin","name","work permit","employer","occupation","date of application","date of expiry","date of issue"]
    dataset={}
    if output_json is not None:
        dataset=output_json
    
    for i in range (0, len(text_data)):
        
        
        data=text_data[i].lower()
        if ("fin" in data or  difflib.SequenceMatcher(None,'fin',data).quick_ratio()>0.6) and i!=len(text_data)-1 and "FIN" not in dataset:
            dataset["FIN"]= text_data[i+1]
        if ("work permit" in data ) and i!=len(text_data)-1 and "FIN" not in dataset and "FIN" not in dataset:
            dataset["FIN"]= text_data[i+1].replace('$','S')  
        elif ("name" == data or difflib.SequenceMatcher(None,'name',data).quick_ratio()>0.7) and i!=len(text_data)-1 and "Name" not in dataset:
            dataset["Name"]= text_data[i+1]
        elif ("employer" == data or difflib.SequenceMatcher(None,'employer',data).quick_ratio()>0.8) and i!=len(text_data)-1 and "Employer" not in dataset:
            dataset["Employer"]= text_data[i+1]
        elif ("occupation" in data ) and i!=len(text_data)-1 and "Occupation" not in dataset:
            dataset["Occupation"]= text_data[i+1]          
        elif  ("date of application" == data or difflib.SequenceMatcher(None,'date of application',data).quick_ratio()>0.8) and i!=len(text_data)-1 and "DateofApplication" not in dataset:
            
            dataset["DateofApplication"]=validateDate(text_data[i+1])
        elif  ("date of expiry" == data or difflib.SequenceMatcher(None,'date of expiry',data).quick_ratio()>0.8) and i!=len(text_data)-1 and "DateofExpiry" not in dataset:
            
            dataset["DateofExpiry"]=validateDate(text_data[i+1])
        elif  ("date of issue" == data or difflib.SequenceMatcher(None,'date of issue',data).quick_ratio()>0.8) and i!=len(text_data)-1 and "DateofIssue" not in dataset:
            
            dataset["DateofIssue"]=validateDate(text_data[i+1])               
         
    print(str(dataset))
    return dataset

def parse_ep_back(text,output_json):
   
    print(text)
    text_data=text.split("\n")
    
    dataset={}
    if output_json is not None:
        dataset=output_json
    
    
    for i in range (0, len(text_data)):
        
        
        data=text_data[i].lower()
       
    
        if  ("sex" == data or difflib.SequenceMatcher(None,'sex',data).quick_ratio()>0.6) and i!=len(text_data)-1 and "Sex" not in dataset:
            if(len(text_data[i+1].split())!=0):
                if(text_data[i+1].split()[-1] =="M"):
                    dataset["Sex"]="Male"
                elif(text_data[i+1].split()[-1] =="F"):
                    dataset["Sex"]="Female"    
        elif  ("sex" in data or difflib.SequenceMatcher(None,'sex',data).quick_ratio()>0.6) and i!=len(text_data)-1 and "Sex" not in dataset:
            values=text_data[i+1].split()
            if(len(values)!=0):
                for value in values:
                    if len(value) ==1 and (value.lower() =="m"):
                        dataset["Sex"]="Male"
                        break
                    elif len(value)==1 and (value.lower()=="f"):
                        dataset["Sex"]="Female"    
                        break
        elif ("fin" in data or  difflib.SequenceMatcher(None,'fin',data).quick_ratio()>0.6) and i!=len(text_data)-1 and "FIN" not in dataset:
            dataset["FIN"]= text_data[i+1].replace('$','S')
        elif  ("nationality" in data or difflib.SequenceMatcher(None,'nationality',data).quick_ratio()>0.6) and i!=len(text_data)-1 and "Nationality" not in dataset:
            if(len(text_data[i+1].split())!=0):
                dataset["Nationality"]=text_data[i+1].split()[-1]        
        elif  ("nationality" == data or difflib.SequenceMatcher(None,'nationality',data).quick_ratio()>0.6) and i!=len(text_data)-1 and "Nationality" not in dataset:
            if(len(text_data[i+1].split())!=0):
                dataset["Nationality"]=text_data[i+1].split()[-1]
        elif  ("date of expiry" in data or difflib.SequenceMatcher(None,'date of expiry',data).quick_ratio()>0.8) and i!=len(text_data)-1 and "DateofExpiry" not in dataset:    
            dataset["DateofExpiry"]=validateDate(text_data[i+1].split()[-1])
        elif  ("date of issue" in data or difflib.SequenceMatcher(None,'date of issue',data).quick_ratio()>0.8) and i!=len(text_data)-1 and "DateofIssue" not in dataset:
            dataset["DateofIssue"]=validateDate(text_data[i+1].split()[-1])  		
         
    print(str(dataset))
    return dataset



def validateDate(dateString):
    
    numbers = sum(c.isdigit() for c in dateString)
    if(numbers==8):
        return dateString
    
    return ""

    


#if __name__=="__main__":
#    parse_nric("REPUBLIC OF SINGAPORE\nIDENTITY CARD NO. S1619407G\nName\nKONG MUN CHEW\n1\nR\nRace\nCHINESE\nDate of Birth\n14-01-1963\nCountry of Birth\nSINGAPORE\n8159807G\nM\n2031269\nNRIC No. S1619407G\nONLINE\nBlood Group Date of issue\nA.. 17-05-1994\n39 GREENVIEW CRESCENT\nSINGAPORE 289344\nNRIC No: S1619407G\n9 Date: 18/10/2017\n")
