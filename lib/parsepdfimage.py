'''
Created on 22-May-2018

@author: anirudh
'''



import fitz

from fitz import csRGB, csGRAY



cmyk_scale = 100
rgb_scale = 255

def cmyk_to_rgb(c,m,y,k):
    """
    """
    r = rgb_scale*(1.0-(c+k)/float(cmyk_scale))
    g = rgb_scale*(1.0-(m+k)/float(cmyk_scale))
    b = rgb_scale*(1.0-(y+k)/float(cmyk_scale))
    return r,g,b
    # rescale to the range [0,cmyk_scale]
    return c*cmyk_scale, m*cmyk_scale, y*cmyk_scale, k*cmyk_scale

def get_image_bytes_pd(pdfbytes):

    
    arr_png_bytes=[]
    png_detected=False
    doc = fitz.open("/tmp/a.pdf",pdfbytes)

    for i in range(0,doc.pageCount):
       
        try:
            page = doc.loadPage(i)
            pix = page.getPixmap(colorspace = fitz.csRGB,alpha = False)
            if (str(pix.colorspace) == str(csRGB)) or (str(pix.colorspace) == str(csGRAY)) or (pix.colorspace is None ):                  # can be saved as PNG
                print("here1")
                print(pix.colorspace)
                
                arr_png_bytes.append(pix.getPNGData())
                
            else:
                print("here2")
                print(csGRAY)
                print(pix.colorspace)                          # must convert CMYK first
                pix0 = fitz.Pixmap(fitz.csRGB, pix)
                arr_png_bytes.append(bytes(pix0.getPNGData()))
        except Exception as e:
            print(str(e))
                  
    if(len(arr_png_bytes)>0):
        png_detected=True
        print(png_detected)
            
    return arr_png_bytes,png_detected


if __name__=="__main__":
    get_image_bytes_pd("")
