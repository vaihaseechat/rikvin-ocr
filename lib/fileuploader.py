'''
Created on 07-Jun-2018

@author: anirudh
'''

import uuid

def handle_uploaded_file(f):
    
    
    new_name=uuid.uuid4().hex+"."+f.name.split(".")[-1]
    filepath='/tmp/'+new_name
    with open(filepath, 'wb+') as destination:
        for chunk in f.chunks():
            destination.write(chunk)
    return filepath         
