'''
Created on 07-Jun-2018

@author: anirudh
'''
from django.shortcuts import render_to_response, render
from OCRService.models import Line
from django.http import HttpResponse
from OCRService.forms import UploadFileForm
from lib.fileuploader import handle_uploaded_file
from lib.ScanDocument import parse_mrz_main,parse_sing_id_front_main,parse_sing_id_back_main,parse_sing_ep_front_main,parse_sing_ep_back_main
from django.http import HttpResponseBadRequest
from django.views.decorators.csrf import csrf_exempt
import os
from ratelimit.decorators import ratelimit
 


@csrf_exempt
@ratelimit(key="ip", rate="5000/d", method=ratelimit.UNSAFE, block=True)
def upload_passport_file(request):
    if request.method == 'POST':
        form = UploadFileForm(request.POST, request.FILES)
        if form.is_valid():
            file_path=handle_uploaded_file(request.FILES["image"])
            ouputjson=parse_mrz_main(file_path)
            os.remove(file_path)
            return HttpResponse(ouputjson)
        else:
          
            return HttpResponseBadRequest('{"Error":"Missing Fields or bad request"}')
    else:
        return HttpResponseBadRequest('{"Error":"Bad request type, must be POST"}')

@csrf_exempt 
@ratelimit(key="ip", rate="5000/d", method=ratelimit.UNSAFE, block=True) 
def upload_nric_front_file(request):
    if request.method == 'POST':
        form = UploadFileForm(request.POST, request.FILES)
        if form.is_valid():
            file_path=handle_uploaded_file(request.FILES["image"])
            ouputjson=parse_sing_id_front_main(file_path)
            os.remove(file_path) 
            return HttpResponse(ouputjson)
        else:
          
            return HttpResponseBadRequest('{"Error":"Missing Fields or bad request"}')
    else:
        return HttpResponseBadRequest('{"Error":"Bad request type, must be POST"}')

@csrf_exempt    
@ratelimit(key="ip", rate="5000/d", method=ratelimit.UNSAFE, block=True) 
def upload_nric_back_file(request):
    if request.method == 'POST':
        form = UploadFileForm(request.POST, request.FILES)
        if form.is_valid():
            file_path=handle_uploaded_file(request.FILES["image"])
            ouputjson=parse_sing_id_back_main(file_path)
            os.remove(file_path)
            return HttpResponse(ouputjson)
        else:
          
            return HttpResponseBadRequest('{"Error":"Missing Fields or bad request"}')
    else:
        return HttpResponseBadRequest('{"Error":"Bad request type, must be POST"}')    
    
@csrf_exempt 
@ratelimit(key="ip", rate="5000/d", method=ratelimit.UNSAFE, block=True)   
def upload_ep_front_file(request):
    if request.method == 'POST':
        form = UploadFileForm(request.POST, request.FILES)
        if form.is_valid():
            file_path=handle_uploaded_file(request.FILES["image"])
            ouputjson=parse_sing_ep_front_main(file_path)
            os.remove(file_path) 
            return HttpResponse(ouputjson)
        else:
          
            return HttpResponseBadRequest('{"Error":"Missing Fields or bad request"}')
    else:
        return HttpResponseBadRequest('{"Error":"Bad request type, must be POST"}')   
		
@csrf_exempt
@ratelimit(key="ip", rate="5000/d", method=ratelimit.UNSAFE, block=True) 
def upload_ep_back_file(request):
    if request.method == 'POST':
        form = UploadFileForm(request.POST, request.FILES)
        if form.is_valid():
            file_path=handle_uploaded_file(request.FILES["image"])
            ouputjson=parse_sing_ep_back_main(file_path)
            os.remove(file_path) 
            return HttpResponse(ouputjson)
        else:
          
            return HttpResponseBadRequest('{"Error":"Missing Fields or bad request"}')
    else:
        return HttpResponseBadRequest('{"Error":"Bad request type, must be POST"}')     
