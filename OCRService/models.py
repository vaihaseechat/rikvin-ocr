'''
Created on 07-Jun-2018

@author: anirudh
'''
from django.db import models

class Line(models.Model):                    # model - class    - table
    text = models.CharField(max_length=255)  # field - instance - row