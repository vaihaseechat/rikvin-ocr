'''
Created on 07-Jun-2018

@author: anirudh
'''
from django import forms

class UploadFileForm(forms.Form):
    
    image = forms.FileField()
