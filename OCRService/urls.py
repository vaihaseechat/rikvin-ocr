"""OCRService URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

from django.urls import path

from OCRService.views import upload_passport_file,upload_nric_front_file,upload_nric_back_file,upload_ep_front_file,upload_ep_back_file

urlpatterns = [
    path('ocr/passport/', upload_passport_file),
    path('ocr/nric-front/', upload_nric_front_file),
    path('ocr/nric-back/', upload_nric_back_file),
    path('ocr/ep-front/', upload_ep_front_file),
    path('ocr/ep-back/', upload_ep_back_file)
]
