'''
Created on 07-Aug-2018

@author: anirudh
'''
from django.http import response

class CorsMiddleware(object):
    def process_response(self, req, resp):
        response["Access-Control-Allow-Origin"] = "*"
        return response

