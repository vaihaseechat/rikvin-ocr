/**
 * http://usejsdoc.org/
 */


$('#post-form').on('submit', function(event){
    event.preventDefault();
     $("#output").text("");
    create_post();
    
});

function create_post() {
	
    var form = $('#post-form')[0]; // You need to use standard javascript object here
    var formData = new FormData(form);
    console.log(formData);
    $.ajax({
        url : "/ocr-submit/", // the endpoint
        type : "POST", // http method
        data : formData, // data sent with the post request
        processData: false,
     
        contentType: false,
        // handle a successful response
        success : function(json) {
           
            console.log(json); // log the returned json to the console
            console.log("success"); // another sanity check
            $("#output").text(json);
            
        },

        // handle a non-successful response
        error : function(xhr,errmsg,err) {
            
            console.log("aya"); // provide a bit more info about the error to the console
        }
    });
    
    
};